package edu.uoc.android.restservice.ui.enter;

import android.app.Application;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Owner;

/**
 * Created by edgardopanchana on 4/29/18.
 */


public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    ArrayList<Owner> listaFollowers;//FOLLOWERS ES EL TIPO DE LISTA

    public AdaptadorFollowers(ArrayList<Owner> listaFollowers) {//en esta linea encontramos el metodo constructor que asigna los datos obtenidos a una variable
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        holder.etiNombre.setText(listaFollowers.get(position).getLogin()); //Linea que realiza  el envio de seguidor al TextView
        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.imageViewLista);//Linea que realiza  el envio de la imagen de seguidor a ImageView
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView etiNombre;
        ImageView imageViewLista;//variable que se usa para obtener ImageView

        public ViewHolderFollowers(View itemView) {
            super(itemView);

            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);
            imageViewLista = (ImageView)itemView.findViewById(R.id.imageViewLista);

        }
    }
}
